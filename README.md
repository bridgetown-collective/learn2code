# learn2code
A quick-start guide (and a work in progress) to going from person on a couch to web developer on a couch :muscle:

## General Resources
- [atom](https://atom.io)
  - a solid text editor provided by GitHub
  - [VS Code](https://code.visualstudio.com/) is another popular one and provided by Microsoft
- [codecademy](https://www.codecademy.com/)
  - for code walkthroughs and tutorials
- [GitLab](https://gitlab.com)
  - a free, private git hosting provider
  - [GitHub](https://github.com) is a more popular one (recently bought by Microsoft)
- [git tutorial](https://backlog.com/git-tutorial/)
  - for getting started with the concepts and commands of git
- [Homebrew](https://brew.sh/)
  - for Mac users to install the things they miss
- [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
  - very clean developer-focused documentation by Mozilla (the creator of Firefox)
  - a lot of Google searches will ends up somewhere on this site
  - this link is to the JavaScript section specifically, but their docs cover much more
- [stackoverflow](https://stackoverflow.com/)
  - when anyone says they "Googled" an answer to programming/coding related question they probably found the answer here
- [w3schools](https://www.w3schools.com/)
  - a quick reference glossary for all things HTML and CSS

## Outline
- [ ] [Section 1: Learn HTML & CSS](sections/section-1.md)
- [ ] [Section 2: Git & Terminal](sections/section-2.md)
- [ ] [Section 3: (Project) Create a resume](sections/section-3.md)
- [ ] [Section 4: JavaScript basics](sections/section-4.md)
- [ ] [Section 5: (Project) Create a timer](sections/section-5.md)
- [ ] [Section 6: jQuery & JS libraries](sections/section-6.md)
- [ ] [Section 7: Bootstrap & CSS libraries](sections/section-7.md)
- [ ] [Section 8: (Project) Create a "Todo App"](sections/section-8.md)
- [ ] [Section 9: AJAX w/ Axios](sections/section-9.md)
- [ ] [Section 10: (Project) Update "Todo App" to save live data](sections/section-10.md)
- [ ] [Section 11: React & "Create React App"](sections/section-11.md)
- [ ] [Section 12: (Project) Create a simple blogging site](sections/section-12.md)

## Notes on how to use this guide
- This is an in progress guide and suggestions are welcomed
- sections are intended (but not required) to be done in order
- consequently, sections start easier and get progressively more difficult
- each section will list out "References" related to it, often including examples
