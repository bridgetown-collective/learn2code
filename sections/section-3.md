### Section 3: (Project) Create a resume

1. Open terminal
2. Navigate to the `~/workspace/learn2code` directory
3. Make sure your in the new branch (e.g. `section-3/john-smith`) created in [Section 2](sections/section-2.md) using `git status`
  - if not, refer back to [Section 2](sections/section-2.md) to create the new branch for this Section
4. Open your text editor and open the `~/workspace/learn2code` project
  - If you don't already have one, [Atom](https://atom.io) is a great place to start
5. Create a new, blank file in the `resumes` folder with your name (e.g. `resume-john-smith.html`)
6. Using Atom or the terminal, add that new file to git, create a commit (your first git commit message :tada:), and `git push` it
7. Open that blank file in the browser
  - Using terminal on a Mac: `open resumes/resume-john-smith.html`
  - Using Finder on a Mac: right-click the file and hit "Open With" to select a browser
  - You're now viewing your blank resume :muscle:
8. Open your resume file in your text editor (Atom) and add some words to it
9. Save the file, go back to the browser, and refresh the page
  - You should see the words you typed show up in the browser
  - This is how you can view your changes in the project below :point_down:

#### Project
Create a resume for yourself using the HTML & CSS learned from [Section 1](sections/section-1.md). It should include at least the following:
- A custom font -- [Google fonts](https://fonts.google.com/) is a great place to look
- A rounded `<img>`
- A list (using either `<ol>` or `<ul>`)
- A demonstration of putting to elements inline
  - could be text next to an image, a two column list, etc.
- A link (`<a>`) with a custom hover state
- Also! Be sure to use proper HTML syntax and indentations
  - Refer to the example resume ([code](resumes/resume-brock-haugen.html) and [live](https://bridgetown-collective.gitlab.io/learn2code/resumes/resume-brock-haugen.html)) for help

Once the resume is finished, add it as a git commit and push it on your branch. Then visit this repo on GitLab (https://gitlab.com/bridgetown-collective/learn2code) and create a "merge request" (also known as a "Pull Request" or "PR").

Once the request is merged you'll be able to see your resume live, online at https://bridgetown-collective.gitlab.io/learn2code/resumes/ `<YOUR RESUME NAME>` .html

Then continue to [Section 4](sections/section-4.md)

#### References
- [Atom](https://atom.io)
- [Example Resume Code](resumes/resume-brock-haugen.html)
- [Example Resume Live](https://bridgetown-collective.gitlab.io/learn2code/resumes/resume-brock-haugen.html)
- ["Make a Website on Codecademy"](https://www.codecademy.com/learn/make-a-website)
