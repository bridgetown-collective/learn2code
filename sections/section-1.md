### Section 1: Learn HTML & CSS

1. Sign up for a free account on [codecademy](https://www.codecademy.com)
2. Complete the following two free courses
  - https://www.codecademy.com/learn/learn-html
  - https://www.codecademy.com/learn/learn-css

Then continue to [Section 2](sections/section-2.md)

#### References
- [codecademy](https://www.codecademy.com/)
- [w3schools](https://www.w3schools.com/)
