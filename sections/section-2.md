### Section 2: Git & Terminal

1. Open your "Terminal"
2. Read over this: https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything
  - learn at least how to move around using `cd`, list files using `ls`, and creating directories with `mkdir`
3. Download and install [Homebrew](https://brew.sh)
4. Install git with `brew install git`
5. Learn git!
  - A few options include:
    - https://backlog.com/git-tutorial/
    - https://www.codecademy.com/learn/learn-git
  - Be sure to know about the base commands such as `git init`, `git add`, `git commit`, and `git push/pull`
  - Also learn basic branching strategy/commands (you'll use this in the next Section)
6. Configure your git setup
  - set your name and email
    - https://backlog.com/git-tutorial/reference/git-config/
    - Run `git config --global credential.helper osxkeychain` to make sure your computer remembers your git credentials
7. Create a [GitLab](https://gitlab.com) account
8. Create a `~/workspace` directory on your computer and `cd` into it
9. Clone this learn2code repository
  - You should now have a `~/workspace/learn2code` folder; `cd` into it
10. Create a new branch for yourself for the next section
  - For instance `git branch -b section-3/john-smith`
11. Run `git push`
  - This will ask for your GitLab username and password the first time
  - You'll now have your own branch listed here: https://gitlab.com/bridgetown-collective/learn2code/branches

Then continue to [Section 3](sections/section-3.md)

#### References
- [GitLab](https://gitlab.com)
- [git tutorial](https://backlog.com/git-tutorial/)
- [Homebrew](https://brew.sh/)
